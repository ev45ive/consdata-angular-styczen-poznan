import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { PlaylistsComponent } from './playlists/playlists.component';
import { ItemsListComponent } from './playlists/items-list.component';
import { ListItemComponent } from './playlists/list-item.component';
import { PlaylistDetailsComponent } from './playlists/playlist-details.component';

import { FormsModule } from '@angular/forms';
import { HighlightDirective } from './playlists/highlight.directive';
import { HideIfDirective } from './playlists/hide-if.directive'
import { MusicModule } from './music/music.module';
import { AuthService } from './auth/auth.service';
import { AuthModule } from './auth/auth.module';
import { Routing, PlaylistResolve } from './app.routing';
import { PlaylistContainerComponent } from './playlists/playlist-container.component';
import { PlaylistsService } from './playlists/playlists.service';

@NgModule({
  declarations: [
    AppComponent,
    PlaylistsComponent,
    ItemsListComponent,
    ListItemComponent,
    PlaylistDetailsComponent,
    HighlightDirective,
    HideIfDirective,
    PlaylistContainerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AuthModule,
    MusicModule,
    Routing
  ],
  providers: [
    PlaylistsService,
    PlaylistResolve
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private auth:AuthService){
    this.auth.getToken()
  }

  // ngDoBootstrap(){

  // }

 }
