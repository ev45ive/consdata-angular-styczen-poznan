import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {
  token:string;

  constructor() { }

  authenticate(){
    const client_id = '5b26982b19ef4499b00c7d7af11b7bff'
    const redirect_uri = 'http://localhost:4200/'
    const url = `https://accounts.spotify.com/pl/authorize?client_id=${client_id}&redirect_uri=${redirect_uri}&response_type=token`
    
    localStorage.removeItem('token')
    window.location.replace(url)
  }
  
  getToken(){
    this.token = JSON.parse(localStorage.getItem('token'))

    if(!this.token){
      const match = window.location.hash.match(/access_token=([^&]*)/)
      this.token = match && match[1]

      localStorage.setItem('token', JSON.stringify(this.token))
      // location.hash = ''
      history.pushState("", document.title, window.location.pathname
    + window.location.search);
    }

    if(!this.token){
      this.authenticate()
    }
    return this.token
  }

}
