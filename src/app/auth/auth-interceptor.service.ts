import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { AuthService } from './auth.service';
import { catchError, retry, retryWhen } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler) {

    const headers = req.headers.append(
      'Authorization', `Bearer ${this.auth.getToken()}`
    )
    var authenticatedRequest = req.clone({ headers })


    const handleError = (err, caught) => {
      this.auth.authenticate()
      return Observable.throw(new Error('ups..'))
    };

    return next.handle(authenticatedRequest)
      .pipe(
        catchError(handleError)
      )
  }

  constructor(private auth: AuthService) {}

}
