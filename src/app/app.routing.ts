import { RouterModule, Routes, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'
import { PlaylistsComponent } from './playlists/playlists.component';
import { MusicSearchComponent } from './music/music-search.component';
import { PlaylistContainerComponent } from './playlists/playlist-container.component';
import { Playlist } from './shared/models/playlist';
import { PlaylistsService } from './playlists/playlists.service';
import { Injectable } from '@angular/core';

@Injectable()
export class PlaylistResolve implements Resolve<Playlist>{

  constructor(private playlistsService:PlaylistsService){}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.playlistsService.getPlaylist(
      route.paramMap.get('playlist_id')
    )
  }

}

const routes: Routes = [
  { path: '', redirectTo: 'playlists', pathMatch: 'full' },
  {
    path: 'playlists',
    children: [
      {
        path:'',
        component: PlaylistsComponent,
      },
      {
        path:':playlist_id',
        component: PlaylistsComponent,
        children: [
          { path: '', component: PlaylistContainerComponent },
        ],
        resolve:{
          playlist: PlaylistResolve,
        },
        data:{
          placki: true
        }
      },
    ]
  },
  { path: 'music', component: MusicSearchComponent },
  { path: '**', redirectTo: 'playlists', pathMatch: 'full' }
]


export const Routing = RouterModule.forRoot(routes, {
  // enableTracing: true,
  // useHash: true,
  // onSameUrlNavigation:'reload',
  // paramsInheritanceStrategy:'always'
})