import { Component, OnInit } from '@angular/core';
import { Playlist } from '../shared/models/playlist';
import { PlaylistsService } from './playlists.service';
import { Router, ActivatedRoute } from '@angular/router';
import { tap, map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-playlists',
  template: `
    <!-- .row>.col*2 -->
    <div class="row">
      <div class="col">
        <app-items-list 
            (selectedChange)="select($event)"
            [selected]="selected$ | async"
            [items]="playlists$ | async">
        </app-items-list>
      </div>
      <div class="col">
          <router-outlet></router-outlet>
      </div>
    </div>
  `,
  styles: []
})
export class PlaylistsComponent implements OnInit {

  selected$;
  playlists$

  select(playlist){
    this.router.navigate(['/playlists', playlist.id])
  }

  constructor(
    private router:Router,
    private route:ActivatedRoute,
    private playlistsService:PlaylistsService) { 
    this.playlists$ = this.playlistsService.getPlaylists()

    this.selected$ =  this.route.data.pipe(
      map(data => data['playlist'])
    )

    // this.selected$ = this.route.firstChild.paramMap.pipe(
    //   map(params => parseInt(params.get('playlist_id')) ),
    //   switchMap(id => this.playlistsService.getPlaylist(+id))
    // )
  }

  ngOnInit() {
  }

}
