import { Directive, Input, TemplateRef, ViewContainerRef, ViewRef } from '@angular/core';

// interface Context<T>{
//   $explicit: T;
//   index: number
//   first: boolean
//   last: boolean
//   // middle: boolean
//   even: boolean
//   odd: boolean
// }


@Directive({
  selector: '[hideIf]'
})
export class HideIfDirective {

  cache:ViewRef

  @Input('hideIf')
  set toggle(hide){
    
    if(hide){
      // this.vcr.clear()
      this.cache = this.vcr.detach()
    }else{
      if(this.cache){
        this.vcr.insert(this.cache)
      }else{
        this.vcr.createEmbeddedView(this.tpl,this.context,0)
      }
    }
  }

  context = {
    $implicit: 'ala ma kota',
    test: 'ala ma test'
  }

  constructor(
    private tpl:TemplateRef<any>,
    private vcr:ViewContainerRef
  ) { 

  }
}
