import { Directive, ElementRef, Input, OnInit, HostListener,HostBinding } from '@angular/core';

@Directive({
  selector: '[highlight]'
})
export class HighlightDirective implements OnInit  {

  ngOnInit() { }

  active = false

  @HostListener('mouseenter',['$event.x','$event.y'])
  onActivate(){
   this.active = true
  }

  @HostListener('mouseleave')
  onDeactivate(){
   this.active = false
  }

  @Input('highlight')
  set highlight(color){
    this.color = color;
  }

  color

  @HostBinding('style.borderLeftColor')
  get calcuatedColor(){
    return this.active? this.color : 'inherit'
  }

  constructor(
    // private elem:ElementRef
  ) {}
}

window['HighlightDirective'] = HighlightDirective
