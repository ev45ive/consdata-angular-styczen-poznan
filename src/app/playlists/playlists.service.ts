import { Injectable } from '@angular/core';
import { Playlist } from '../shared/models/playlist';
import { of } from 'rxjs/observable/of';
import { tap } from 'rxjs/operators';

@Injectable()
export class PlaylistsService {

  playlists: Playlist[] = [
    {
      id: 1,
      name: 'Angular The Best Of',
      favourite: false,
      color: '#ff0000'
    },
    {
      id: 2,
      name: 'Angular Greatest Hits',
      favourite: true,
      color: '#00ff00'
    },
    {
      id: 3,
      name: 'Angular TOP20',
      favourite: false,
      color: '#0000ff'
    },
  ]

  getPlaylists() {
    return of(this.playlists)
  }

  getPlaylist(id) {
    return of(this.playlists.find(playlist => playlist.id == id)).pipe(
      tap(console.log),
    )
  }

  // TODO: FIXME: !!!
  savePlaylist(playlist:Playlist){
    return of(playlist)
  }

  constructor() { }
}
