import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { PlaylistsService } from './playlists.service';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-playlist-container',
  template: `
    <div *ngIf="(playlist$ | async) as playlist; else noResult">
      <app-playlist-details [playlist]="playlist">
      </app-playlist-details>
    </div>

    <ng-template #noResult>
      Please select playlist
    </ng-template>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlaylistContainerComponent implements OnInit {

  playlist$

  constructor(
    private route:ActivatedRoute,
    private playlistsService: PlaylistsService
  ) {
    // const id = this.route.snapshot.paramMap.get('playlist_id')

    this.playlist$ = this.route.data.pipe(
      map(data => data['playlist'])
    )

    // this.playlist$ = this.route.paramMap.pipe(
    //   // tap(console.log),
    //   map(params => parseInt(params.get('playlist_id')) ),
    //   switchMap(id => this.playlistsService.getPlaylist(+id))
    // )
  }

  ngOnInit() {
  }

}
