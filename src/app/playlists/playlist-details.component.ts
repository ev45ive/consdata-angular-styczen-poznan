import { Component, OnInit, Input } from '@angular/core';
import { Playlist } from '../shared/models/playlist';

@Component({
  selector: 'app-playlist-details',
  template: `
    <div *ngIf=" mode == 'edit' "> 
      <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control" 
            [(ngModel)]="playlist_draft.name" >
      </div>
      <div class="form-group">
        <label>Favourite</label>
        <input type="checkbox"  
              [(ngModel)]="playlist_draft.favourite">
      </div>
      <div class="form-group">
        <label>Color</label>
        <input type="color" [(ngModel)]="playlist_draft.color">
      </div>
      <button class="btn btn-danger" (click)="cancel()">Cancel</button>
      <button class="btn btn-success" (click)="save()">Save</button>
    </div>

    <div *ngIf="mode == 'show' ">
      <label>Name:</label>
      <p> {{playlist.name}} </p>
     
      <label>Favourite:</label>
      <p> {{playlist.favourite? 'Yes' : 'No' }} </p>
     
      <label>Color:</label>
      <p [style.color]="playlist.color"> {{playlist.color}} </p>

      <button class="btn btn-info" (click)="edit()">Edit</button>
    </div>
  `,
  styles: [`label{font-weight:bold;}`,
// ` :host > div{   border-left: 10px solid black;}`
]
})
export class PlaylistDetailsComponent implements OnInit {

  @Input()
  playlist:Playlist

  playlist_draft:Playlist

  mode = 'show' 

  cancel(){
    this.mode = 'show'
  }

  save(){
    // Object.assign(this.playlist, this.playlist_draft)
  }

  edit(){
    this.mode = 'edit'
    // Object.assign({}, this.playlist)
    // this.playlist_draft = { 
    //   ...this.playlist
    // }
    this.playlist_draft = this.playlist 
  }

  constructor() { }

  ngOnInit() {
  }

}
