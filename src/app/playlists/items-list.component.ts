import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { Playlist } from '../shared/models/playlist';

// (mouseenter)=" hover = playlist "
// (mouseleave)=" hover = false "
// [style.borderLeftColor]="(playlist == hover ? playlist.color : 'inherit') "

@Component({
  selector: 'app-items-list',
  template: `
    <div class="list-group">
      <div class="list-group-item"
          *ngFor="let playlist of items; let i = index"
          (click)=" select( playlist ) "
          
          [highlight]="playlist.color"
          
          [style.fontSize.px]="playlist.favourite && size"
          [class.active]=" playlist == selected ">
        {{i+1}}. {{playlist.name}}
      </div>
    </div>
  `,
  styles: [`
    .list-group-item{
      border-left: 10px solid black;
    }
  `]
})
export class ItemsListComponent implements OnInit {

  hover:Playlist

  size = 12

  @Input()
  selected: Playlist;

  @Output()
  selectedChange = new EventEmitter<Playlist>()

  @Input()
  items: Playlist[]

  select(playlist: Playlist) {
    this.selectedChange.emit(playlist)
    // this.selected = playlist
  }

  constructor() { }

  ngOnInit() {
  }

}
