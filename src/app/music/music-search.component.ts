import { Component, OnInit } from '@angular/core';
import { MusicService } from './music.service';
import { Album } from '../shared/models/album';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-music-search',
  template: `
  <div class="row">
    <div class="col">
      <app-search-form
      [query]="queries$ | async "
      (queryChange)="search($event)"></app-search-form>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <app-albums-list [albums]="albums$ | async"></app-albums-list>
    </div>
  </div>
  `,
  // providers:[
  //   MusicService
  // ],
  styles: []
})
export class MusicSearchComponent implements OnInit {

  albums$: Observable<Album[]>
  queries$: Observable<string>
  
  constructor(private musicService: MusicService) {
    this.albums$ = this.musicService.getAlbums()
    this.queries$ = this.musicService.queries$
  }
  
  search(query){
    this.musicService.search(query)
  }

  ngOnInit() {
    // this.musicService
    // .getAlbums()
    // .subscribe(data => {
    //   this.albums = data
    // })
  }

}
