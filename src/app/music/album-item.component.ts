import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Album } from '../shared/models/album';
import { AlbumImage } from '../shared/models/album-image';

// .card>img.card-img-top[src]+.card-body>h5.card-title{Text}

@Component({
  selector: 'app-album-item, [app-album-item] ',
  template: `
    <img class="card-img-top" [src]="image.url">
    <div class="card-body">
    <!-- 
      <input type="range" min="2" max="30" [(ngModel)]="len"> -->
      <h5 class="card-title">{{album.name | shorten }}</h5>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: []
})
export class AlbumItemComponent implements OnInit {

  @Input('album')
  set onAlbum(album){
    this.album = album
    const SIZE = 0;
    this.image = album.images[SIZE]
  }

  // ngDoCheck(){
  //   console.log('check')
  //   this.cdr.markForCheck()
  // }

  album:Album

  image:AlbumImage

  len = 20;

  constructor(private cdr:ChangeDetectorRef) {
    // setInterval(()=>{
    //   this['len']++
    // },500)

   }

  ngOnInit() {
    
  }

}
