import { Component, OnInit, EventEmitter, Output, Input, ChangeDetectionStrategy } from '@angular/core';
import { MusicService } from './music.service';
import { FormGroup, AbstractControl, FormControl, FormArray, Validators, ValidatorFn, AsyncValidatorFn, Validator, ValidationErrors } from '@angular/forms';
import { filter, distinctUntilChanged, debounceTime, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { PartialObserver } from 'rxjs/Observer';
import { combineLatest } from 'rxjs/operators/combineLatest';
import { withLatestFrom } from 'rxjs/operators/withLatestFrom';

@Component({
  selector: 'app-search-form',
  template: `
    <form class="form-group mb-3" [formGroup]="queryForm">
    <input type="text" class="form-control" placeholder="Search query" autofocus formControlName="query">
    <div *ngIf="queryForm.pending">Please wait... Checking...</div>
    <ng-container *ngIf="queryForm.get('query').dirty || queryForm.get('query').touched">
        <p *ngIf="queryForm.get('query').hasError('required')">Field is required</p>
        <p *ngIf="queryForm.get('query').hasError('minlength')">Minimum {{queryForm.get('query').getError('minlength').requiredLength}} letters required</p>
        <p *ngIf="queryForm.get('query').hasError('censor')">Cant use word  "{{queryForm.get('query').getError('censor')}}" </p>
    </ng-container>
    </form>
  `,
  // changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [`
  form .ng-invalid.ng-touched,
  form .ng-invalid.ng-dirty {
        border: 2px solid red !important;
    }
  `]
})
export class SearchFormComponent implements OnInit {

  queryForm: FormGroup

  @Output()
  queryChange // = new EventEmitter<string>()

  @Input('query')
  set onQuery(query) {
    this.queryForm.setValue({ query }, { emitEvent: false })
  }

  constructor() {

    const censor = (badword): ValidatorFn => (control: AbstractControl) => {
      const error = control.value === badword
      return <ValidationErrors | null>(error ? { censor: badword } : null)
    }

    const asyncCensor = (badword): AsyncValidatorFn => (control: AbstractControl) => {
      // return this.http.post(server_validation_url, data ).map( response => validationErrors | null )
      
      return Observable.create((observer:PartialObserver<ValidationErrors|null>) => {

        var handler = setTimeout(() => {
          const error = control.value === badword

          observer.next(<ValidationErrors | null>(error ? { censor: badword } : null))
          observer.complete()
        }, 2000)

        // on unsubscribe:
        return () => { clearTimeout(handler) }
      })
    }

    this.queryForm = new FormGroup({
      'query': new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        // censor('batman')
      ],[
        asyncCensor('batman')
      ])
    })
    // this.queryChange = this.queryForm.get('query').valueChanges.pipe(
    //   debounceTime(400),
    //   filter(({ length }) => length >= 3),
    //   distinctUntilChanged(),
    //   // tap( console.log )
    // )

    const isValid$ = this.queryForm.get('query').statusChanges.pipe(
      filter(status => status === "VALID")
    )
    const value$ = this.queryForm.get('query').valueChanges

    this.queryChange = isValid$.pipe(withLatestFrom(value$,(_,value)=> value)).pipe(
      debounceTime(400),
      distinctUntilChanged()
    )


    window['form'] = this.queryForm

  }


  // search(query){
  //   this.queryChange.emit(query)
  // }

  ngOnInit() { }

}
