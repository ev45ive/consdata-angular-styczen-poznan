import { Injectable, Inject } from '@angular/core';
import { Album } from '../shared/models/album';
import { HttpClient } from '@angular/common/http'
import { AuthService } from '../auth/auth.service';

type AlbumsResponse = {
  albums: {
    items: Album[]
  }
}

// import 'rxjs/Rx'
// import 'rxjs/add/operator/pluck'
import { pluck, map, catchError, filter, flatMap, mergeMap, switchMap, mapTo } from 'rxjs/operators'
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class MusicService {

  // albums:Album[] = []
  albums$ = new BehaviorSubject<Album[]>([])
  queries$ = new BehaviorSubject<string>('alice')

  constructor(private http: HttpClient, private auth: AuthService) {
    const notEmpty = ({ length }) => length >= 1

    this.queries$
      .pipe(
        switchMap(query => notEmpty(query)? this.request(query) : of([]))
      )
      .subscribe(this.albums$)
  }

  request(query:string){
    const queryToUrl = query => `https://api.spotify.com/v1/search?type=album&q=${query}`;
    const request = url => this.http.get<AlbumsResponse>(url)

    return of(query).pipe(
      map(queryToUrl),
      switchMap(request),
      map(response => response.albums.items),
    )
  }

  search(query) {
    this.queries$.next(query);
  }

  getAlbums(): Observable<Album[]> {
    return this.albums$.asObservable()
  }

}
