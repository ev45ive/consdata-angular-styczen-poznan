import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicSearchComponent } from './music-search.component';
import { SearchFormComponent } from './search-form.component';
import { AlbumsListComponent } from './albums-list.component';
import { AlbumItemComponent } from './album-item.component';
import { environment } from '../../environments/environment';
import { MusicService } from './music.service';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ShortenPipe } from './shorten.pipe';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule 
  ],
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    AlbumsListComponent,
    AlbumItemComponent,
    ShortenPipe
  ],
  providers:[
    {
      provide:'albums_data',
      useValue: [{
        id: '123',
        name:'Test',
        artists:[{
          id:'134', name: 'Testowy'
        }],
        images:[{
          url: 'http://via.placeholder.com/300x300',
          height: 300,
          width: 300
        }]
      }]      
    },
    {
      provide:'albums',
      useFactory: (data)=>{
        return data
      },
      deps: ['albums_data']
    },
    // {
    //   provide: MusicService,
    //   useClass: MusicService
    // },
    MusicService
  ],
  exports:[
    MusicSearchComponent
  ]
})
export class MusicModule { }
