import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorten',
  // pure: false
})
export class ShortenPipe implements PipeTransform {

  transform(value: string, maxLen = 20): any {
    // return Math.random()
    return value.length > maxLen ? 
      (value.substr(0, maxLen - 3) + '...') : value
  }

}