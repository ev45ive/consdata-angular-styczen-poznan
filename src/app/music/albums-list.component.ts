import { Component, OnInit, Inject, Input, ChangeDetectionStrategy } from '@angular/core';
import { Album } from '../shared/models/album';
import { MusicService } from './music.service';

@Component({
  selector: 'app-albums-list',
  template: `
  <div class="card-group">
    <div app-album-item [album]="album" class="card" 
          *ngFor="let album of albums"></div>
  </div>
  `,
  styles: [`
    .card {
      flex-basis:25%;
      flex-grow:0;
    }
  `],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlbumsListComponent implements OnInit {

  @Input()
  albums: Album[]

  constructor() {}

  ngOnInit() {}

}
