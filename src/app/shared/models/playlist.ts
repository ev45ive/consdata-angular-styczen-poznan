
export interface Playlist{
  id: number;
  name: string;
  /**
   * Is really favourite!?
   */
  favourite: boolean;
  /**
   Color you like 
   */
  color: string;
  // tracks: Array<Track>
  tracks?: Track[]
}

interface Track{ }