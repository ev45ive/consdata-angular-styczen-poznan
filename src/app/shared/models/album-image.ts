export interface AlbumImage{
  url: string;
  height: number;
  width: number;
}