type UUID = string;

export interface Entity{  
    id: UUID;
    name: string;
}