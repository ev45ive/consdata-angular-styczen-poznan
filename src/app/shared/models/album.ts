import { Entity } from "./entity";
import { Artist } from "./artist";
import { AlbumImage } from "./album-image";

export interface Album extends Entity {
  images: AlbumImage[];
  artists: Artist[];
}